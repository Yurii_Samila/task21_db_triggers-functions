USE storedpr_db;
-- SELECT * FROM INFORMATION_SCHEMA.TRIGGERS;
 DROP TRIGGER insert_medicine_zone;
 DROP PROCEDURE insert_into_employee;
 DROP FUNCTION MinExperience;

-- INSERT INTO employee VALUES (1, 'Cruz','Tom', 'Amazing', 1, 'PS1245', 5.4, 19750218, 'Actor', 10);
-- INSERT INTO employee VALUES (2, 'Love','Samantha', 'Cool', 1, 'PS4444', 5.0, 19701109, 'Singer', 35);
-- INSERT INTO employee VALUES (3, 'Matew','Eight', 'WTF', 1, 'PS8888', 5.4, 19880808, 'Driver', 88);
-- INSERT INTO employee VALUES (4, 'Johnny','Bob', 'SomeNew', 8, 'PS7800', 4.1, 19991212, 'Singer', 2);
-- INSERT INTO employee VALUES (5, 'Leiva','Lucas', 'Unknow', 5, 'PS0009', 9.8, 19770111, 'Doctor', 3);
-- INSERT INTO street VALUES ('Zelena');
-- INSERT INTO post VALUES ('Doctor');
-- UPDATE post SET post = 'wewe' WHERE post ='Doc[tor';
-- INSERT INTO medicine VALUES (2, 'AbraCadabra', 'ry-478-89', 1, 0, 0);
-- INSERT INTO pharmacy VALUES (1, 'Pharmacy1', 4544, 'www.phph', '19:00:00', 1, 0, 'Zelena');
-- INSERT INTO pharmacy VALUES (2, 'Pharmacy2', 47, 'www.ttt', '10:00:00', 0, 0, 'Kopernyka');
-- INSERT INTO pharmacy VALUES (3, 'Pharmacy3', 0062, 'www.asd', '01:00:00', 0, 1, 'Svobody');
-- INSERT INTO pharmacy_medicine VALUES(1,1);
-- UPDATE employee SET post = 'Singer' WHERE id = 1;
-- INSERT INTO post VALUES('Actor');
-- INSERT INTO post VALUES('Singer');
-- DELETE FROM pharmacy_medicine WHERE pharmacy_id IN(1);
-- DELETE FROM post WHERE post = 'Actor';
DELETE FROM medicine_zone WHERE medicine_id IN(1,2,3,4,5);
CALL insert_into_employee(3, 'Matew','Eight', 'WTF', 1, 'PS8888', 5.7, 19880808, 'Singer', 1);
DELIMITER //
CREATE PROCEDURE insert_into_employee(
v1 int,
v2 VARCHAR(30),
v3 CHAR(10),
v4 VARCHAR(30),
v5 CHAR(10),
v6 CHAR(10),
v7 DECIMAL(10,1),
v8 DATE,
v9 VARCHAR(15),
v10 int)
BEGIN
INSERT INTO employee VALUES (v1,v2,v3,v4,v5,v6,v7,v8,v9,v10);
END //
DELIMITER ;

CALL insert_into_medicine_zone(2,3);
DELIMITER //
CREATE PROCEDURE insert_into_medicine_zone(v1 int, v2 int)
BEGIN
INSERT INTO medicine_zone VALUES (v1,v2);
END //
DELIMITER ; 

SELECT *, MinExperience() FROM employee;

DELIMITER //
CREATE FUNCTION MinExperience()
RETURNS DECIMAL(10,1)
BEGIN
RETURN (SELECT MIN(experience) FROM employee);
END //
DELIMITER ;

SELECT FindAdress(2);

DELIMITER //
CREATE FUNCTION FindAdress(idValue int)
RETURNS VARCHAR(30)
BEGIN
RETURN (SELECT CONCAT(name, " ", building_number) FROM pharmacy WHERE id = idValue);
END //
DELIMETER ;

DELIMITER //
CREATE TRIGGER insert_employee
BEFORE INSERT
ON employee FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM post WHERE post = new.post) = 0
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'No such post! Can''t insert without correct foreign key!';
ELSEIF (SELECT COUNT(*) FROM pharmacy WHERE id = new.pharmacy_id) = 0
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'No such pharmacy! Can''t insert without correct foreign key!';
ELSEIF new.identity_number RLIKE '00$'
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Identity number can''t end with "00"';
END IF;
END //
DELIMITER ;		

DELIMITER //
CREATE TRIGGER update_employee
BEFORE UPDATE
ON employee FOR EACH ROW
BEGIN
IF new.post NOT IN(SELECT post FROM post)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'No such post! Can''t update without correct foreign key!';
ELSEIF new.pharmacy_id NOT IN(SELECT id FROM pharmacy)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'No such pharmacy! Can''t update without correct foreign key!';
ELSEIF new.identity_number RLIKE '00$'
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Identity number can''t end with "00"';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER update_post
BEFORE UPDATE
ON post FOR EACH ROW
BEGIN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'You can''t change table "Post"!!!';
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER delete_post
BEFORE DELETE
ON post FOR EACH ROW
BEGIN
IF old.post IN(SELECT post FROM employee)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Can''t delete post. This post is a foreign key';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER delete_pharmacy
BEFORE DELETE
ON pharmacy FOR EACH ROW
BEGIN
IF old.id IN(SELECT pharmacy_id FROM employee) OR old.id IN(SELECT pharmacy_id FROM pharmacy_medicine)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Can''t delete pharmacy. This pharmacy is a foreign key';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER insert_pharmacy
BEFORE INSERT
ON pharmacy FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM street WHERE street = new.street) = 0
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'No such street! Can''t insert without correct foreign key!';
END IF;
END //
DELIMITER ;

INSERT INTO medicine VALUES (2, 'AbraCadabra', 'eeea', 1, 0, 0);
DELETE FROM medicine WHERE id = 2;
DELIMITER //
CREATE TRIGGER insert_medicine
BEFORE INSERT
ON medicine FOR EACH ROW
BEGIN
IF (new.ministry_code NOT RLIKE '[[:alpha:]]{2,2}')
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Wrong format!';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER update_pharmacy
BEFORE UPDATE
ON pharmacy FOR EACH ROW
BEGIN
IF new.street NOT IN(SELECT * FROM street)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'No such street! Can''t update without correct foreign key!';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER delete_street
BEFORE DELETE
ON street FOR EACH ROW
BEGIN
IF old.street IN(SELECT street FROM pharmacy)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Can''t delete street. This street is a foreign key!';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER delete_medicine
BEFORE DELETE
ON medicine FOR EACH ROW
BEGIN
IF old.id IN(SELECT medicine_id FROM pharmacy_medicine) OR old.id IN(SELECT medicine_id FROM medicine_zone)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Can''t delete medicine. This medicine is a foreign key';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER delete_zone
BEFORE DELETE
ON zone FOR EACH ROW
BEGIN
IF old.id IN(SELECT zone_id FROM pharmacy_medicine)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Can''t delete zone. This zone is a foreign key';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER insert_pharmacy_medicine
BEFORE INSERT
ON pharmacy_medicine FOR EACH ROW
BEGIN
IF new.pharmacy_id NOT IN(SELECT id FROM pharmacy) OR new.medicine_id NOT IN(SELECT id FROM medicine)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Can''t insert! No such entities';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER insert_medicine_zone
BEFORE INSERT
ON medicine_zone FOR EACH ROW
BEGIN
IF new.zone_id NOT IN(SELECT id FROM zone) OR new.medicine_id NOT IN(SELECT id FROM medicine)
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Can''t insert! No such entities';
END IF;
END //
DELIMITER ;
